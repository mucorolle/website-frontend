import _ from "lodash";

export default (profilesByName) => {
  const {
    "twitter-en": twitterEnProfile,
    "twitter-ru": twitterRuProfile,
    ...otherProfiles
  } = profilesByName;

  const statusCountEn = _.get(twitterEnProfile, ["info", "statusCount"]);
  const statusCountRu = _.get(twitterRuProfile, ["info", "statusCount"]);
  if (Number.isFinite(statusCountEn) && Number.isFinite(statusCountRu)) {
    return {
      twitter: {
        name: "twitter",
        info: {
          statusCountEn,
          statusCountRu,
        },
      },
      ...otherProfiles,
    };
  }
  return otherProfiles;
};
