import compression from "compression";
import express from "express";
import next from "next";
import nextI18NextMiddleware from "next-i18next/middleware";
import { format, parse } from "url";

import { env } from "./config";
import { nextI18next } from "./i18n";

const app = next({ dev: env.NODE_ENV !== "production" });
const handle = app.getRequestHandler();

(async () => {
  await app.prepare();
  const server = express();
  server.use(nextI18NextMiddleware(nextI18next));

  server.use(compression());

  // redirect legacy CV links
  server.get(
    ["/cv", "/cv_uk", "/cv/uk", "/CVs/Alexander_Kachkaev_CV_UK.pdf"],
    (_, res) => {
      res.redirect("https://www.linkedin.com/in/kachkaev");
    },
  );

  // handle any other requests
  server.get("*", (req, res) => {
    const { pathname, query } = parse(req.url, true);

    // remove trailing slash
    if (
      pathname.length > 1 &&
      pathname.slice(-1) === "/" &&
      pathname.indexOf("/_next/") !== 0
    ) {
      return res.redirect(
        format({
          pathname: pathname.slice(0, -1),
          query,
        }),
      );
    }

    // pass any other requests to next
    (req as any).hostByLocale = {
      en: env.HOST_EN,
      ru: env.HOST_RU,
    };
    (req as any).gaTrackingId = env.GA_TRACKING_ID;
    (req as any).graphqlUri = env.GRAPHQL_URI;
    (req as any).nodeEnv = env.NODE_ENV;
    return handle(req, res);
  });

  server.listen(env.PORT, (err) => {
    if (err) {
      throw err;
    }
    console.log(`> Ready on port ${env.PORT}`);
  });
})();
