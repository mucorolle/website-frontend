import { createGlobalStyle } from "styled-components";
import normalize from "styled-normalize";

import base from "./base";
import nbprogress from "./nbprogress";

const GlobalStyle = createGlobalStyle`
  ${normalize}
  ${base}
  ${nbprogress}
`;

export default GlobalStyle;
