import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { UrlObject } from "url";

import { HostByLocale } from "../types";
import Locale from "./Locale";

const Wrapper = styled.div`
  position: absolute;
  top: 12px;
  right: 25px;
`;

const LocaleToggler: FunctionComponent<{
  hostByLocale: HostByLocale;
  url: UrlObject;
  children?: never;
}> = ({ hostByLocale, url }) => {
  return (
    <Wrapper>
      <Locale targetLocale="en" host={hostByLocale["en"]} currentUrl={url} />{" "}
      <Locale targetLocale="ru" host={hostByLocale["ru"]} currentUrl={url} />
    </Wrapper>
  );
};

export default LocaleToggler;
