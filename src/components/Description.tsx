import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { Trans } from "../i18n";

const Wrapper = styled.div`
  padding-top: 12px;
  padding-bottom: 28px;
`;

const Description: FunctionComponent<{ children?: never }> = () => {
  return (
    <Wrapper>
      <Trans i18nKey="index:description.l1" />
      <br />
      <Trans i18nKey="index:description.l2">
        <a href="http://www.gicentre.net/">giCentre</a>
      </Trans>
      <br />
      <Trans i18nKey="index:description.l3" />
    </Wrapper>
  );
};

export default Description;
