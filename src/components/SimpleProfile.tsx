import React, { FunctionComponent } from "react";
import styled from "styled-components";

const Wrapper = styled.span`
  display: inline-block;
  padding: 0 8px;
`;

const Link = styled.a<{ notUsed: boolean }>`
  ${(p) => (p.notUsed ? "text-decoration: line-through;" : "")};
`;

const SimpleProfile: FunctionComponent<{
  href: string;
  name: string;
  notUsed?: boolean;
  children?: never;
}> = ({ href, name, notUsed = false }) => {
  return (
    <Wrapper>
      <Link notUsed={notUsed} href={href}>
        {name}
      </Link>
    </Wrapper>
  );
};

export default SimpleProfile;
