import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { useTranslation } from "../i18n";

const Wrapper = styled.div`
  float: right;
`;
const Img = styled.img`
  display: block;
  border-radius: 5px;
  color: #fff;
`;

const Photo: FunctionComponent<{ children?: never }> = () => {
  const { t } = useTranslation();
  return (
    <Wrapper>
      <Img
        width="100"
        height="100"
        alt={t("index:photoAlt")}
        src="/images/alexander_kachkaev.jpg?"
      />
    </Wrapper>
  );
};

export default Photo;
