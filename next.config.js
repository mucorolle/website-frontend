/* eslint-disable @typescript-eslint/no-var-requires */

module.exports = require("next-compose-plugins")(
  [
    require("@next/bundle-analyzer")({
      enabled: process.env.ANALYZE === "true",
    }),
  ],
  {
    experimental: {
      modern: true,
    },
    webpack: (config, { isServer }) => {
      const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
      config.plugins.push(new LodashModuleReplacementPlugin({ paths: true }));
      if (!isServer) {
        config.externals = {
          "i18next-browser-languagedetector": "{}",
          "i18next-express-middleware": "{}",
          "./config": "{}",
        };
      }
      return config;
    },
  },
);
